using UnityEngine;

public class FallProjectileMovement : DefaultProjectileMovement
{
    // Start is called before the first frame update
    void Start()
    {
        projectileRigidbody = GetComponent<Rigidbody>();
        profileData = GetComponent<Projectile>().profileData;
        pointLine = GetComponent<LineRenderer>();
        projectileRigidbody.velocity = transform.forward * profileData.currentSpeed;
    }

    private void Update()
    {
        Vector3 startPos = transform.position;
        Vector3 endPos = startPos;
        endPos.y -= 100;
        pointLine.SetPosition(0, startPos);
        pointLine.SetPosition(1, endPos);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            IDamageable damageable = other.GetComponent<IDamageable>();
            damageable.OnDamage(profileData.damage, new Vector3(0, 0, 0), new Vector3(0, 0, 0));
            return;
        }

        if (other.tag == "Player Zone")
        {
            Destroy(transform.gameObject);
        }

        Bounds(other);
    }

    LineRenderer pointLine;
}

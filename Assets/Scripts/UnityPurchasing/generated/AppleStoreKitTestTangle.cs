// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleStoreKitTestTangle
    {
        private static byte[] data = System.Convert.FromBase64String("8DgECbhQuUtDIXJGyZ6qiPz0uzqiiQITKix2n1ivKJ3X8bVpl7ol2tI27ZoeKa6uXuPES358RknkWwCXRHfrOzw7/jRSSfu+9V9cupjDv3Cfm8Dn/OH22PrnopijmpWQxpeVgJmfm8Dn/OH22PrnooKjnJWQxpeYkcbAooSjhpWauRXbFWSekpqShZt0/HOIdZ77Xrb9UOWbZtmYPennr7GjnpWauRXbFWSekpKSlpOQEZKco5mVm7iVkpaWlJCQo56VmrkV2xVT1LvYeMlSuJrFvzY5mpDooaJPe7kV2xVknpKSmJaTo8yigqOclZDG2AAomWsysq1b8kq3W82M5q1JAEiBEEUU6/4KLs0gmXMLXWV5UB3++mSekpKYlpOQEZKSkyGTc69ie3rxCmggGcYMfI67K8y5QuAMptSkKNX0tio4/6D2PktRCevbnHpfUrSeVZeQn5vA5/zh9tj656KCo5yVkMaXXOs/zimtzMO4z7ofTlFEuYRMsSdXOpggXPGcaCcvZEgXzbt41J5ifh9y4kjZ1mC+igWQxRI1133m5MjAXPev/hPKoB9ZpZW5goge2l2bbPXnooKjnJWQxpeZn5vA5/zh9tj656ahoKbJhJ6no6OgoaSipKahoKbJ8+WkwX9sP4rOn4a8GZxmGltEuI6igqOclZDGl5ifm8Dn/OH22PrnolFodcyBUpGQkpOSMKijqqOclZDGwOf84fbY+uejjYSeoaOjp6OioqSToxGSmZERkpKTSOwDr7/gnEgEtb/mdd8igUBucIzNnu4ry0Qz73fsmKrfWNz9UyDMJAYpKjvO2ec4fPyPfJob6/ZdEmEe4XSUif21gWShIkDlg2Fj+n9e8QriavMZF9urVy4Y2xVknpKakoWbwOf84fbY+uejEZIW69tdU1aBgvmcnz28lln86ffss54oqwP4lVdvnnYGw14CWym2Z8taoxGQ56MRkc8zkJGSkZGSkqOelZoYP2YMI5qrMP3oWLXDmYlyGkLh5aPMooKjnJWQxpeQn5vA5/zh9tj6kmyXl5CRkRejhZWQxo62kpJsl5+OgJKSbJeWo5CSkmyjnZWQxo6ckngk0xKuFK7jwkJXzh42T+z5y4lNmKOalZDGl5WAkcbAooSjhpWauRUuU0WO0w3EKqsD5OXBQTvY4/0r9I1/uAKWNK7c");
        private static int[] order = new int[] { 40,16,21,26,4,26,26,17,13,30,37,16,15,41,31,33,35,21,25,25,24,36,30,36,43,37,27,27,31,39,43,43,34,33,38,36,40,37,41,39,43,42,43,43,44 };
        private static int key = 147;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}

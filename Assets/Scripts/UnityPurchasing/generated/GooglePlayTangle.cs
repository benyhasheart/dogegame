// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("dgmrkOu5LALBhW+/JjzPlFKuDMIhuMiXcR44WgwqlIMMrsYeS9Fov7I64pRRQwzkqp6y2W91J6i/KiEnZcwbrdj0NLPjPtwOloxpN2tHVL4KkdM7jM9Ly36vKkvra+I0gi42SX6P1nkgZ0FxJ3dglAEoPmaIhRWEkCKhgpCtpqmKJugmV62hoaGloKOpeRSd6BVa+t0BKSelLncbfC2XCM3/FxidqMb+TaXSV6W9mj/muPdxDhkc9yjLD114aIYDjBFoXhocZhLyVn5RkE6yuFBwyr3ijBME1VTB1CKhr6CQIqGqoiKhoaAd5lIe30P8Du0LOc75m+l1/ywpNtagHuWDr3S7lYFV9IGK4N2yoV4W66Kp0R0bj1k3GyCzrqisfaKjoaCh");
        private static int[] order = new int[] { 6,7,13,8,4,13,6,9,8,12,13,12,12,13,14 };
        private static int key = 160;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILoader : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnEnableUI(bool enable)
    {
        ui.SetActive(enable);
    }

    public void OnPasue(bool pause)
    {
        GameManager.GameManagerInstance.PauseGame(pause);
    }

    
    [SerializeField]
    GameObject ui;


}

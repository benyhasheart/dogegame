using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour, IUseable
{
    public virtual void OnUse()
    {

    }

    public virtual void OnUse(GameObject target)
    {

    }
}

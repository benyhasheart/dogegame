using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using System;

public class GameManager : MonoBehaviour
{

    private void Awake()
    {
        instance = this;
        bulletCount = 0;
        //gameLevel = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        isGameover = false;
        start = false;
        surviveTime = 0.0f;
        // 플레이어 델리게이트 등록
        player = FindObjectOfType<PlayerCharacter>();

        if ( !ReferenceEquals(player, null))
        {
            player.onDeath += EndGame;
        }
        //nextlevel Check
        StartCoroutine(SetUpCoroutine());
        //NextLevel();
    }

    // Update is called once per frame
    void Update()
    {
        if ( !isGameover && start)
        {
            surviveTime += Time.deltaTime;
            UIManager.UIManagerInstance.SetTimeTextValue(surviveTime);
            UIManager.UIManagerInstance.SetBulletCountTextValue(bulletCount);            
        }

        
        //if (checkNextTime >= nextLevelTime)
        //{
        //    checkNextTime = 0.0f;
        //    NextLevel();
        //}

        //checkNextTime += Time.deltaTime;
        //move to eventManager                

    }

    public void EndGame()
    {
        
        isGameover = true;
        PauseGame(true);

        float bestTime = PlayerPrefs.GetFloat("BestTime");

        if ( surviveTime > bestTime)
        {
            bestTime = surviveTime;

            PlayerPrefs.SetFloat("BestTime", bestTime);
        }
        UIManager.UIManagerInstance.SetRecoredTimeValue(bestTime);
        UIManager.UIManagerInstance.SetResultTimeTextValue(surviveTime);
        //ui manager setup GameOverUI
        UIManager.UIManagerInstance.GameOver();

        GoogleAdsManager.instance.AdStart();
    }

    public void RestartGame()
    {
        PauseGame(false);
        UIManager.UIManagerInstance.Clear();
        SceneManager.LoadScene("GameScene");      
    }

    public void PauseGame(bool active)
    {
        if ( active)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1f;
        }

    }

    public void addBulletCount(int value = 1)
    {
        bulletCount += value;
    }

    public static GameManager GameManagerInstance
    {
        get { return instance; }
    }

    public int GameLevel
    {
        get { return gameLevel; }
    }

    public PlayerCharacter Player
    {
        get { return player; }
    }
    IEnumerator SetUpCoroutine()
    {
        WaitForSeconds waitTime = new WaitForSeconds(0.5f);

        while (true)
        {
            bool ready = false;
            foreach(BulletSpawner spawner in spawnerList)
            {
                if ( spawner.Ready)
                {
                    ready = true;
                }
            }

            if (ready)
            {
                //start
                StartCoroutine(NextLevelCoroutine());
                start = true;
                break;
            }

            yield return waitTime;
        }
    }
    IEnumerator NextLevelCoroutine()
    {
        WaitForSeconds waitTime = new WaitForSeconds(nextLevelTime);

        while (true)
        {
            onNextLevel();
            gameLevel += 1;
            UIManager.UIManagerInstance.SetLevelValue(gameLevel);
            yield return waitTime;
        }

    }

    private void NextLevel()
    {
        onNextLevel();
        gameLevel += 1;
        UIManager.UIManagerInstance.SetLevelValue(GameLevel);
    }

    //ui text
    public TextMeshProUGUI timeText;
    public TextMeshProUGUI recordText;
    public TextMeshProUGUI bulletCountText;


    //spawner
    public List<BulletSpawner> spawnerList;
    //spawnData

    public List<SpawnProjectileData> spawnDataList;
    //delegate
    public Action onNextLevel;

    public float nextLevelTime;

    [SerializeField]
    private int gameLevel;

    private static GameManager instance;
    private PlayerCharacter player;

    private int bulletCount;
    private float surviveTime;


    private bool isGameover;
    private bool start;
}

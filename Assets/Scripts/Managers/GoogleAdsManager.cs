using UnityEngine;
using GoogleMobileAds.Api;
using UnityEngine.Advertisements; 
using System;
using System.Collections.Generic;

public class GoogleAdsManager : MonoBehaviour
{
    private InterstitialAd interstitial;

    public void Start()
    {
        instance = this;
        DontDestroyOnLoad(this);
        //광고 초기화
        MobileAds.Initialize(initStatus =>
        {
            RequestInterstitial();
        });            
        

        //AdStart();
    }
    private void RequestInterstitial()
    {
        //여러 OS에서 공통된 코드를 사용할 경우 이렇게 하면 편리
        //여기 들어가는 ID는 /가 들어간 쪽의 광고 단위 ID
        //이 ID들은 Google이 지원하는 테스트 ID이므로 제한 없이 사용 가능
        
        string adUnitId = "ca-app-pub-2389342610321901/8592737789";
#if UNITY_ANDROID
        //string adUnitId = "ca-app-pub-3940256099942544/1033173712";
        //adUnitId = "ca-app-pub-2389342610321901/8592737789";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#elif UNITY_EDITOR
            string adUnitId = "unexpected_platform";
#else
            string adUnitId = "unexpected_platform";
#endif

        //단일 OS일 경우 여기서 바로 스트링으로 꽂아줘도 가능
        this.interstitial = new InterstitialAd(adUnitId);

        this.interstitial.OnAdLoaded += HandleOnAdLoaded;
        this.interstitial.OnAdFailedToShow += HandleOnAdFailedToShow;

        AdRequest request = new AdRequest.Builder().Build();
        //test device
        List<string> deviceIds = new List<string>();
        deviceIds.Add("73891d6f-8a71-44fc-bcc4-cdc1633e7571");
        RequestConfiguration requestConfiguration = new RequestConfiguration
            .Builder()
            .SetTestDeviceIds(deviceIds)
            .build();

        MobileAds.SetRequestConfiguration(requestConfiguration);
        //
        this.interstitial.LoadAd(request);

    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("OnLoaded");
    }
    public void HandleOnAdFailedToShow(object sender, EventArgs args)
    {
        MonoBehaviour.print("AdFailedToShow");
    }

    //광고를 시작해야 할 때에 외부에서 이 함수를 호출
    public void AdStart()
    {
        if (this.interstitial.IsLoaded())
        {            
            this.interstitial.Show();
        }
        else
        {
            RequestInterstitial();
        }
        
        
    }

    public static GoogleAdsManager instance;
}
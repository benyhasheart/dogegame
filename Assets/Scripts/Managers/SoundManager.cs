using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
    }
    // Start is called before the first frame update
    void Start()
    {
        soundDictionary.Clear();
        soundDictionary.Add(soundType.sfx, sfxList);
        soundDictionary.Add(soundType.bgm, bgmList);
    }
    //sfx list
    [SerializeField]
    List<AudioClip> sfxList;
    //music list
    [SerializeField]
    List<AudioClip> bgmList;
    //generic 
    [SerializeField]
    Dictionary<soundType, List<AudioClip>> soundDictionary = new Dictionary<soundType, List<AudioClip>>();
    public static SoundManager instance;
    private SoundVolume sfxVolume;
    private SoundVolume musicVolume;
}

public class SoundVolume
{
    public SoundVolume()
    {
        currentValue = 0.0f;
    }

    public void SetVolume(float value)
    {
        currentValue = Mathf.Clamp(value, min, max);
    }

    public float GetVolume()
    {
        return currentValue;
    }

    private float currentValue;
    private readonly float min = 0.0f;
    private readonly float max = 1.0f;

}

public enum soundType
{
    sfx = 0,
    bgm,
}
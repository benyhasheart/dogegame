using UnityEngine;

public interface IUseable 
{
    void OnUse();
    void OnUse(GameObject target);
}
